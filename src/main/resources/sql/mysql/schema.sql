﻿# Host: 172.16.30.121  (Version: 5.1.69-log)
# Date: 2015-01-23 15:34:19
# Generator: MySQL-Front 5.3  (Build 4.75)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "restadmin"
#

DROP TABLE IF EXISTS `restadmin`;
CREATE TABLE `restadmin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bucket` varchar(50) NOT NULL,
  `tableName` varchar(50) NOT NULL DEFAULT '' COMMENT '表名',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '字段名',
  `aliasName` varchar(50) DEFAULT NULL COMMENT '字段别名',
  `type` int(3) DEFAULT '0' COMMENT '字段类型',
  `minLength` int(1) DEFAULT '0',
  `maxLength` int(5) DEFAULT '0' COMMENT '字段长度',
  `request` tinyint(2) DEFAULT '0' COMMENT '是否为空',
  `displayType` int(3) DEFAULT '1' COMMENT '显示类型',
  `displayValue` varchar(100) DEFAULT NULL COMMENT '显示值',
  `sort` int(3) DEFAULT '0' COMMENT '字段顺序',
  `searchType` int(3) DEFAULT '0' COMMENT '查询字段类型',
  `validate` varchar(100) DEFAULT NULL COMMENT '验证值',
  `formType` int(3) DEFAULT '1',
  `formValue` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bucket_tablename_name` (`bucket`,`tableName`,`name`)
) ENGINE=MyISAM AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

#
# Structure for table "restapp"
#

DROP TABLE IF EXISTS `restapp`;
CREATE TABLE `restapp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL COMMENT '应用名称',
  `createTime` datetime DEFAULT '2000-01-01 00:00:00' COMMENT '创建时间',
  `bucket` varchar(20) DEFAULT '' COMMENT '应用标识',
  `user` varchar(255) DEFAULT NULL COMMENT '应用级脚本',
  `dbType` int(2) DEFAULT '0' COMMENT '数据库类型0默认1mysql2mongo',
  `dbDriverClass` varchar(30) DEFAULT NULL COMMENT '数据库驱动',
  `dbURL` varchar(80) DEFAULT NULL COMMENT '数据库地址',
  `dbUsername` varchar(20) DEFAULT NULL COMMENT '数据库用户名',
  `dbPassword` varchar(20) DEFAULT NULL COMMENT '数据库密码',
  `secretKey` varchar(20) DEFAULT NULL COMMENT '密钥',
  `status` int(2) DEFAULT '1' COMMENT '1 可用 0 禁用',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=158 DEFAULT CHARSET=utf8;

#
# Structure for table "restfield"
#

DROP TABLE IF EXISTS `restfield`;
CREATE TABLE `restfield` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bucket` varchar(20) DEFAULT NULL COMMENT '标识',
  `methodId` int(11) DEFAULT NULL COMMENT '方法id',
  `fieldName` varchar(20) DEFAULT NULL COMMENT '字段名',
  `request` smallint(2) DEFAULT '0' COMMENT '必填 0选填 1必填',
  `regular` varchar(50) DEFAULT NULL COMMENT '正则',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

#
# Structure for table "restgroup"
#

DROP TABLE IF EXISTS `restgroup`;
CREATE TABLE `restgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bucket` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `desc` varchar(50) DEFAULT NULL,
  `roles` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rest_group_bucket_name` (`bucket`,`name`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

#
# Structure for table "restmethod"
#

DROP TABLE IF EXISTS `restmethod`;
CREATE TABLE `restmethod` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bucket` varchar(20) DEFAULT NULL COMMENT '应用标识',
  `name` varchar(20) DEFAULT NULL COMMENT '方法名称',
  `option` varchar(10) DEFAULT NULL COMMENT '操作类型',
  `version` varchar(10) DEFAULT NULL COMMENT '版本',
  `seconds` int(3) DEFAULT '-1' COMMENT '缓存时间',
  `script` varchar(4000) DEFAULT NULL COMMENT '脚本',
  `status` int(2) DEFAULT '1' COMMENT '状态',
  `acl` varchar(200) DEFAULT NULL COMMENT '权限',
  `paramType` smallint(3) DEFAULT '1' COMMENT '参数类型1任意2定义3无参',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=182 DEFAULT CHARSET=utf8;

#
# Structure for table "restrole"
#

DROP TABLE IF EXISTS `restrole`;
CREATE TABLE `restrole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bucket` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `desc` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

#
# Structure for table "resttable"
#

DROP TABLE IF EXISTS `resttable`;
CREATE TABLE `resttable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bucket` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL COMMENT '表名',
  `restAcl` varchar(500) DEFAULT NULL COMMENT 'rest权限',
  `tableAcl` varchar(500) DEFAULT NULL COMMENT '表权限',
  `fieldAcl` varchar(500) DEFAULT NULL COMMENT '字段权限',
  `one2many` varchar(200) DEFAULT NULL,
  `many2one` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=149 DEFAULT CHARSET=utf8;

#
# Structure for table "resttimer"
#

DROP TABLE IF EXISTS `resttimer`;
CREATE TABLE `resttimer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bucket` varchar(20) NOT NULL,
  `type` int(3) DEFAULT '1' COMMENT '1单次2月3周4日5小时',
  `day` int(2) DEFAULT NULL,
  `week` int(2) DEFAULT NULL,
  `hour` int(2) DEFAULT NULL,
  `minutes` int(2) DEFAULT NULL,
  `methodId` varchar(11) DEFAULT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

#
# Structure for table "restuser"
#

DROP TABLE IF EXISTS `restuser`;
CREATE TABLE `restuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bucket` varchar(20) DEFAULT NULL COMMENT '标识',
  `tableName` varchar(20) DEFAULT NULL COMMENT '用户表名',
  `usernameField` varchar(20) DEFAULT NULL COMMENT '用户名字段',
  `passwordField` varchar(20) DEFAULT NULL COMMENT '密码字段',
  `statusField` varchar(20) DEFAULT NULL COMMENT '用户状态',
  `emailAuth` smallint(3) DEFAULT '1' COMMENT '邮箱认证',
  `defaultGroup` varchar(50) DEFAULT NULL,
  `emailField` varchar(20) DEFAULT NULL COMMENT '邮箱字段',
  `mobileField` varchar(20) DEFAULT NULL COMMENT '手机字段',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

#
# Structure for table "restuseracl"
#

DROP TABLE IF EXISTS `restuseracl`;
CREATE TABLE `restuseracl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bucket` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `groups` varchar(100) DEFAULT NULL,
  `roles` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rest_user_acl_bucket_username` (`bucket`,`username`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

#
# Structure for table "userinfo"
#

DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE `userinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `status` int(3) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `qq` varchar(20) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `company` varchar(50) DEFAULT NULL,
  `zipcode` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;
