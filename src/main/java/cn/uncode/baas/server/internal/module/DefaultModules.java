package cn.uncode.baas.server.internal.module;

import cn.uncode.baas.server.internal.module.acl.IAclModule;
import cn.uncode.baas.server.internal.module.data.IDataModule;
import cn.uncode.baas.server.internal.module.mail.IMailModule;
import cn.uncode.baas.server.internal.module.user.IUserModule;
import cn.uncode.baas.server.internal.module.utils.IUtilsModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultModules implements IModules {
	
	@Autowired
	private IDataModule proxyDataModule;
	
	@Autowired
	private IUserModule user;
	
	@Autowired
	private IUtilsModule utils;
	
	@Autowired
	private IMailModule mail;
	
	@Autowired
	private IAclModule acl;

	public IDataModule getData() {
		return proxyDataModule;
	}
	
	public IUserModule getUser() {
		return user;
	}

	public void setUser(IUserModule user) {
		this.user = user;
	}

	public IUtilsModule getUtils() {
		return utils;
	}

	public void setUtils(IUtilsModule utils) {
		this.utils = utils;
	}

	public IMailModule getMail() {
		return mail;
	}

	public void setMail(IMailModule mail) {
		this.mail = mail;
	}

	public IAclModule getAcl() {
		return acl;
	}

	public void setAcl(IAclModule acl) {
		this.acl = acl;
	}

	
}
